# KML Style Importer
Process KML files and imports style definitions into QGIS (support is still partial).

## Developement
The following steps can be used to setup the project for testing and developing:

### Dependencies
The following software is needed to develop and build the puglin
```
apt install python3-sphinx
apt install qttools5-dev-tools
```

### Virtualenv
Setup your pyhon environment
```
apt install python-virtualenv

virtualenv -p python3 env
source env/bin/activate
pip3 install pyqt5
```

### Edit Qt dialogs
Use Qt designer to edit the .ui files
```
designer -qt5
```

### Preview Qt dialogs
You can run the following command in order to preview ui dialogs
```
pyuic5 -p "<path to .ui file>"
```

### Deploy
Deploy and test in your local QGIS 3 installation
```
make deploy
```
Open plugin manager in QGIS, then search for this plugin and enable it.
