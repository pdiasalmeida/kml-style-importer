[general]
name=KML Style Importer
qgisMinimumVersion=3.0
description=Extract style definitions from KML files
version=0.9.0
author=Geomaster
email=geral@geomaster.pt

about=This plugin allows you to process KML files and import their style definitions into QGIS.

tracker=https://gitlab.com/geomaster-qgis/kml-style-importer/issues
repository=https://gitlab.com/geomaster-qgis/kml-style-importer.git

tags=python,KML,sld,style

homepage=https://geomaster.pt
category=Plugins
icon=icon.png

experimental=True
deprecated=False
